﻿using Microsoft.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaPaPa.Web.Common.Auths.QQ
{
    public class QQAccountAuthenticationOptions : AuthenticationOptions
    {
        public QQAccountAuthenticationOptions(PathString pathString)
            : base("QQ")
        {
            this.Caption = "QQ";
            this.CallbackPath = pathString;
            base.AuthenticationMode = AuthenticationMode.Passive;
            this.Scope = new List<string>();
            this.BackchannelTimeout = TimeSpan.FromSeconds(60.0);
        }
        
        public string AppId { get; set; }
        
        public string AppSecret { get; set; }
        
        public ICertificateValidator BackchannelCertificateValidator { get; set; }
        
        public TimeSpan BackchannelTimeout { get; set; }
        
        public PathString CallbackPath { get; set; }
        
        public string Caption
        {
            get
            {
                return base.Description.Caption;
            }
            set
            {
                base.Description.Caption = value;
            }
        }
        
        public IList<string> Scope { get; private set; }
        
        public string SignInAsAuthenticationType { get; set; }
        
        public ISecureDataFormat<AuthenticationProperties> StateDataFormat { get; set; }
    }
}
