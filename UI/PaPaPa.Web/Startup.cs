﻿using Framework.Common.LogOperation;
using Microsoft.Owin;
using Owin;
using PaPaPa.Web.Business.Caches;

[assembly: OwinStartupAttribute(typeof(PaPaPa.Startup))]
namespace PaPaPa
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);

            //注册文本日志
            Logger.RegisterTextWriterListener();

            try
            {
                //初始化缓存
                CacheBusiness.Init();
            }
            catch (System.Exception ex)
            {
                Logger.Error(ex.ToString());
            }
        }
    }
}
