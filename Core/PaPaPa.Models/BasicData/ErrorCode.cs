﻿using Framework.Common.EnumOperation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaPaPa.Models.BasicData
{
    /// <summary>
    /// 错误代码
    /// </summary>
    public enum ErrorCode
    {
        /// <summary>
        /// 找不到用户名
        /// </summary>
        [Extension(Description = "找不到用户名")]
        CanNotFoundUserName = 10000,

        /// <summary>
        /// 密码错误
        /// </summary>
        [Extension(Description = "密码错误")]
        PasswordError = 10001,

        /// <summary>
        /// 用户名已存在
        /// </summary>
        [Extension(Description = "用户名已存在")]
        UserNameExists = 10002,

        /// <summary>
        /// 用户Id不存在
        /// </summary>
        [Extension(Description = "用户Id不存在")]
        UserIdIsNotExists = 10003,
    }
}
